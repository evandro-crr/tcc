#!/bin/python
from qsystem import QSystem, Gate
from random import randint, random, sample, seed

gate = Gate()

gate.make_cgate('ck1', 'IIIXXXXI', [7]) #
gate.make_cgate('ck2', 'XIXIXIXI', [7]) #  
gate.make_cgate('ck3', 'IXXIIXXI', [7]) #  Estabilizadores
gate.make_cgate('ck4', 'IIIZZZZI', [7]) #     do código           
gate.make_cgate('ck5', 'ZIZIZIZI', [7]) #
gate.make_cgate('ck6', 'IZZIIZZI', [7]) #

def prepare(q, cks, gate):
    an_bits = []
    for ck in cks:
        q.add_ancillas(1)
        q.an_evol('H', 0)
        q.evol(ck, 0)
        q.an_evol('H', 0)
        q.an_measure(0)
        an_bits.append(q.get_an_bits()[0])
        q.rm_ancillas()
    i = sum([x[0]*x[1] for x in zip(an_bits, [4, 1, 2])])-1
    if i != -1:
        q.evol(gate, i)
    return an_bits

def prepare_z(q): # Corrige phase flip
    return prepare(q, ['ck1', 'ck2', 'ck3'], 'Z')

def prepare_x(q): # Corrige bit fip
    return prepare(q, ['ck4', 'ck5', 'ck6'], 'X')

err = 0
seed(42)
for i in range(1000):
    q = QSystem(7, i, gate, 'mix')
    pre_bits = prepare_z(q) # Prepara no estado lógico |0>
#             ┌phase flip┐  ┌ bit flip ┐
    qerror = [randint(0,6), randint(0,6)] # qbit sortiado
    p =      [random(),     random()]     # p do erro 
    q.flip('Z', qerror[0], p[0])      
    q.flip('X', qerror[1], p[1]) 
    z_bits = prepare_z(q) 
    x_bits = prepare_x(q) 
    q.measure_all()
    print(i, pre_bits, z_bits, x_bits,
          q.get_bits(), sum(q.get_bits())%2)
    print('phase flip on qbit ', qerror[0] , 'with p =', p[0],
    '(error not detected)' if z_bits == [0, 0, 0] else '(error detected)')
    print('bit flip on qbit', qerror[1] , 'with p =', p[1],
    '(error not detected)' if x_bits == [0, 0, 0] else '(error detected)')
    err += sum(q.get_bits())%2
print('Erros =', err)


