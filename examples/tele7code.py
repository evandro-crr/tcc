#!/bin/python
from qsystem import QSystem, Gate
from random import randint, random, sample, seed

ks = ['IIIXXXX',
      'XIXIXIX',
      'IXXIIXX',
      'IIIZZZZ',
      'ZIZIZIZ',
      'IZZIIZZ']

def make_gate(size):
    gate = Gate()

    for i in range(size):
        for k in ks:
            ki = k + str().join(['IIIIIII' for _ in range(size-i-1)]) + 'I'
            print(k+str(i), ki)
            gate.make_cgate(k + str(i), ki, [len(ki)-1]) 

    return gate

def prepare(q, cks, gate, qbit):
    an_bits = []
    for ck in cks:
        q.add_ancillas(1)
        print('add an')
        q.an_evol('H', 0)
        print('an H')
        q.evol(ck, qbit*7)
        print(ck)
        q.an_evol('H', 0)
        print('an H')
        q.an_measure(0)
        print('an M')
        an_bits.append(q.get_an_bits()[0])
        q.rm_ancillas()
        print('rm an')
    i = sum([x[0]*x[1] for x in zip(an_bits, [4, 1, 2])])-1
    if i != -1:
        q.evol(gate, 7*qbit+i)
    return an_bits

def prepare_z(q, qbit): # Corrige phase flip
    return prepare(q, [k + str(qbit) for k in ks[:3]], 'Z', qbit)

def prepare_x(q, qbit): # Corrige bit fip
    return prepare(q, [k + str(qbit) for k in ks[3:]], 'X', qbit)

seed(42)
n = 3
gate = make_gate(n);
# |0>  ─┤X├───────┬─┤H├─┤M╞══════╗
# |7>  ─────┤H├─┬─X─────┤M╞══╗   ║
# |14> ─────────X───────────┤X├─┤Z├─
#  0     1   2  3 4  5   6   7   8
for i in range(1):
    q = QSystem(7*n, i, gate, 'pure')
    for i in range(n):
        print(prepare_z(q, i)) 
    
    q.evol('X', 0, 7)              # 1
    print('#1')
    q.evol('H', 7, 14)             # 2
    print('#2')
    for i in range(7):
        q.cnot(14+i, [7+i])        # 3
    print('#3')
    for i in range(7):
        q.cnot(7+i, [i])           # 4
    print('#4')
    q.evol('H', 0, 7)              # 5
    print('#5')
    q.measure(0, 14)               # 6
    print('#6')
    if sum(q.get_bits()[7:14])%2:
        q.evol('X', 14, 21)        # 7
    print('#7')
    if sum(q.get_bits()[:7])%2:
        q.evol('Z', 14, 21)        # 8
    print('#8')

    q.measure_all()
    for i in range(n):
        print(sum(q.get_bits()[i*7:(i+1)*7])%2)


