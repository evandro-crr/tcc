#!/bin/python
from qsystem import QSystem, Gate
from random import randint, random, choice, seed

gat = Gate()

gat.make_cgate('pr1', 'XIZIZ', [1])
gat.make_cgate('pr2', 'XZIZI', [4])
gat.make_cgate('pr3', 'ZIZIX', [3])
gat.make_cgate('pr4', 'IZIXZ', [2])

def prepare(q):
    q.evol('Z', 0)
    for pr in zip(['pr1', 'pr2', 'pr3', 'pr4'], [1, 4, 3, 2]):
        q.evol('H', pr[1])
        q.evol(pr[0], 0)

gat.make_cgate('ck1', 'IZXXZI', [5])
gat.make_cgate('ck2', 'ZIZXXI', [5])
gat.make_cgate('ck3', 'XZIZXI', [5])
gat.make_cgate('ck4', 'XXZIZI', [5])

syn = { 1:('Z',2),  2:('X',0),  3:('Z',3),  4:('X',3),  5:('X',1),
        6:('Z',4),  7:('Y',3),  8:('Z',1),  9:('X',4), 10:('X',2),
       11:('Y',2), 12:('Z',0), 13:('Y',1), 14:('Y',0), 15:('Y',4)}

def correction(q):
    an_bits = []
    for ck in ['ck1', 'ck2', 'ck3', 'ck4']:
        q.add_ancillas(1)
        q.an_evol('H', 0)
        q.evol(ck, 0)
        q.an_evol('H', 0)
        q.an_measure(0)
        an_bits.append(q.get_an_bits()[0])
        q.rm_ancillas()
    i = sum([x[0]*x[1] for x in zip(an_bits, [1, 2, 4, 8])])
    if i != 0:
        error = syn[i]
        q.evol(error[0], error[1])
    return an_bits

err = 0
seed(42)
for seed in range(100):
    q = QSystem(5, seed, gat, 'mix')
    prepare(q) 
    fgate = choice(['X', 'Y', 'Z'])
    qbit = randint(0, 4)
    p = random()
    q.flip(fgate, qbit, p)
    an = correction(q)
    q.measure_all()
    print(fgate, qbit, an, q.get_bits(), sum(q.get_bits())%2, p, seed)
    err += sum(q.get_bits())%2
print('Erros =', err)


