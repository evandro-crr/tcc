\chapter{Álgebra linear com notação de Dirac}\label{ch:apA}

A álgebra linear é uma das principais ferramentas da computação quântica.
Portanto, sua compreendimento é fundamental. Neste apêndice é feita uma rápida
revisão de álgebra linear utilizando a notação de Dirac, também conhecida como
notação \textit{braket}.

A notação de Dirac é notação padrão usada na mecânica quântica e, é muito
conveniente quando se trabalha com espaços vetoriais complexos, como é o caso
da computação quântica.

\begin{table}[h!]
  \centering
  \begin{tabular}{c l}
    \toprule
    Notação & Descrição \\
    \midrule
    $z^*$ & Conjugado de $z \in \mathbb{C}$ \\ 
    $A^T$ & Transposto matriz $A$ \\ 
    $A^*$ & Conjugado matriz $A$ \\ 
    $A^\dagger$ & Transposto Hermitiano da matriz $A$, $A^\dagger = {(A^T)}^*$ \\ 
    $\ket{\psi}$ & Vetor coluna chamado \textit{ket} \\
    $\bpsi$ & Vetor dual de $\ket{\psi}$ chamado \textit{bra}, $\bpsi =
    \ket{\psi}^\dagger$\\
    $\braket{\varphi|\psi}$ & Produto interno entre $\ket{\varphi}$ e
    $\ket{\psi}$ \\
    $\ket{\varphi}\otimes\ket{\psi}$ & Produto tensorial entra $\ket{\varphi}$ e
    $\ket{\psi}$ \\
    $\ket{\varphi}\ket{\psi}$ & Produto tensorial entra $\ket{\varphi}$ e
    $\ket{\psi}$ \\
    $\ket{\varphi\psi}$ & Produto tensorial entra $\ket{\varphi}$ e
    $\ket{\psi}$ \\
    \bottomrule
  \end{tabular}
  \caption{Resumo da notação de Dirac}\label{tab:dirac}
\end{table}

\section{Números complexos}
Na computação quântica, representamos um qubit como um vetor pertencente a um
espaço complexo, ou seja, um espaço vetorial sobre o corpo $\mathbb{C}$.

Um número $z\in\mathbb{C}$ é dividido em duas partes, $z = a + bi$, a parte
real $a$ e a parte imaginária $b$ que é multiplicado pela constante $i =
\sqrt{-1}$, onde $i^2 = -1$.

Sendo $z,w\in\mathbb{C}$ e $z = a+bi$, $w = c+di$, define-se:
\begin{description}
  \item [Identidade]
    \begin{align}
      z = w \Leftrightarrow a = c \land b = d;
    \end{align}
  \item [Soma]
    \begin{align}
      z + w  = (a+c) + (b+d)i;
    \end{align}
  \item [Produto]
    \begin{align}
      zw  = (a+bi)(c+di) = (ac-bd)+(bc+ad)i;
    \end{align}
  \item [Conjugado]
    \begin{align}
      z^* = a-bi;
    \end{align}
  \item [Módulo]
    \begin{align}
      |z| = \sqrt{a^2+b^2}.
    \end{align}   
\end{description}

Um número complexo pode ser descrito na forma de Euler como 
\begin{align}
  re^{i\theta} = r\cos{\theta}+r\sin{\theta}i.
\end{align}   

\section{Base e independência linear}

Uma base é um conjunto de vetores linearmente independentes que geram o 
um espaço vetorial.

Um exemplo de base é a base computacional composto pelos vetores 
\begin{equation}
\ket{0} = \KZ \text{\@ e \@} \ket{1} = \KO.
\end{equation}

Todo vetor pertencente ao espaço pode ser gerado pela combinação linear dos
vetores da base. Assim sendo, $\ket{0},\ket{1},\dots,\ket{n}$ uma base e
$\alpha_0,\alpha_1,\dots,\alpha_n\in\mathbb{C}$, um vetor $\ket{\psi}$ qualquer
pode ser descrito como 
\begin{align}
  \ket{\psi} = \alpha_0\ket{0}+\alpha_1\ket{1}+\cdots+\alpha_n\ket{n}
\end{align}

Um conjunto de vetores são linearmente independentes se (sendo 0 o vetor nulo)
\begin{align}
  0 = \alpha_0\ket{0}+\alpha_1\ket{1}+\cdots+\alpha_n\ket{n} 
  \Leftrightarrow \sum_i \alpha_i = 0
\end{align}

\section{Produto interno}

O produto interno é uma operação entre dois vetores que retorna um escalar,
sendo representada na notação de Dirac com um $\braket{bra|ket}$

O produto escalar entre os vetores $\ket{\psi}$ e $\ket{\varphi}$ possui as
seguintes propriedades:
\begin{description}
  \item [Associatividade] 
    \begin{equation}
      \alpha\braket{\varphi|\psi} = (\alpha\ket{\varphi},\ket{\psi}), \quad
      \alpha \in \mathbb{C};
    \end{equation}
  \item [Simetria hermitiana]
    \begin{equation}
      \braket{\varphi|\psi} = \braket{\psi|\varphi}^*;
    \end{equation}
  \item [Positividade]
    \begin{align}
      &\braket{\psi|\psi} > 0, \quad \forall \ket{\psi} \neq 0\\
      &\braket{\psi|\psi} = 0  \Leftrightarrow  \ket{\psi} = 0;
    \end{align}
  \item [Distributividade]
    \begin{equation}
      (\bra{\upsilon}+\bra{\varphi})\ket{\psi} = \braket{\upsilon|\psi} +
      \braket{\varphi|\psi}
    \end{equation}
\end{description}

Dois vetores $\ket{\psi}$ e $\ket{\varphi}$ são linearmente independentes se e
somente se 
\begin{align}
  \braket{\psi|\varphi} = 0.
\end{align}

O produto interno pode ser definido de varias formas, porém, será usada a
seguinte definição para espaços complexos $\mathbb{C}^n$: seja $\ket{\varphi} =
(\nu_1, \nu_2, \dots, \nu_n)$ e $\ket{\psi} = (\upsilon_1, \upsilon_2, \dots,
\upsilon_n)$
\begin{equation}\label{eq:prod_inter}
  \braket{\varphi|\psi} = \LV \CV  = \sum_{i} \nu_i^*\upsilon_i.
\end{equation}

A norma de um vetor $\ket{\psi}$ é dada como
\begin{equation}
  \|\ket{\psi}\| = \sqrt{\braket{\psi|\psi}}.
\end{equation}

\section{Produto externo}

O produto externo é uma operação entre dois vetores, $\ket{\varphi} =
(\upsilon_1, \upsilon_2, \dots, \upsilon_n)$ e $\ket{\psi} = (\nu_1, \nu_2,
\dots, \nu_n)$, que resulta em uma matriz, definida como
\begin{equation}
  \ket{\varphi}\mkern-7mu\bra{\psi} = \CV\LV = \OPm.
\end{equation}

A representação em produto externo de matizes pode ser conveniente na
multiplicação delas, como por exemplo, a multiplicação das matrizes de Pauli X
e Z (vista na Subsecção~\ref{subsec:pauli}) 
\begin{align}
  XZ & = (\ket{1}\mkern-7mu\bra{0} + \ket{0}\mkern-7mu\bra{1})(\ket{0}\mkern-7mu\bra{0} - \ket{1}\mkern-7mu\bra{1})\\
  XZ & = \ket{1}\mkern-7mu\braket{0|0}\mkern-7mu\bra{0} - \ket{1}\mkern-7mu\braket{0|1}\mkern-7mu\bra{1} +
  \ket{0}\mkern-7mu\braket{1|0}\mkern-7mu\bra{0} - \ket{0}\mkern-7mu\braket{1|1}\mkern-7mu\bra{1}\\
  XZ & = \ket{1}\mkern-7mu\bra{0} - \ket{0}\mkern-7mu\bra{1}.
\end{align}

\section{Operação traço}\label{sec:traco}

A operação traço é  definida com sendo o somatório dos elementos da diagonal de
uma matriz, sendo o traço da matriz $A$ igual a 
\begin{equation}
  \tr(A) = \sum_i A_{ii}.
\end{equation}
E o traço da matriz
$\ket{\varphi_0}\mkern-7mu\bra{\psi_1}+\cdots+\ket{\varphi_n}\mkern-7mu\bra{\psi_n}$ igual a 
\begin{equation}
  \tr(\ket{\varphi_0}\mkern-7mu\bra{\psi_1}+\cdots+\ket{\varphi_n}\mkern-7mu\bra{\psi_n}) =
  \braket{\psi_0|\varphi_0}+\cdots+\braket{\psi_n|\varphi_n}.
\end{equation}

Sendo $A$ e $B$ matrizes, o traço possua as seguintes propriedades:
\begin{description}
  \item [Cíclico]
    \begin{equation}
      \tr(AB) = \tr(BA)
    \end{equation}
  \item [Linearidade]
    \begin{equation}
      \tr(A+B) = \tr(A) + \tr(B)
    \end{equation}
  \item [Associatividade]
    \begin{equation}
      z\tr(A) = \tr(zA),\quad z \in \mathbb{C}
    \end{equation}
\end{description}

