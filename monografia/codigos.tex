%   ___           _ _        _
%  / __|__ _ _ __(_) |_ _  _| |___
% | (__/ _` | '_ \ |  _| || | / _ \
%  \___\__,_| .__/_|\__|\_,_|_\___/
%           |_|
\chapter{Correção de erros quânticos}\label{ch:apcode}

Neste apêndice veremos técnicas para identificar e corrigir erros em um sistema
quântico. Em linhas gerais, para construir um qubit tolerante a erros,
codificamos $k$ qubits lógicos em $n$ qubits físicos, com uma distância $d$
entre cada estado lógico. Com isso, conseguimos corrigir $d{-}1\over2$ erros\@.
Podemos denotar cada código de correção de erros a partir de suas
características como $[[n,k,d]]$.

Primeiramente, o código de repetição $[[3,1,3]]$ é apresentado como uma introdução
a correção de erros (Secção~\ref{sec:cod3}). Em seguida, é apresentado o código de
Shor $[[9,1,3]]$ (Secção~\ref{sec:codshor}), que consegue corrigir até dois erros.
Por fim, vemos o formalismo do código estabilizador
(Secção~\ref{sec:estabilizador}), exemplificado nos códigos de 7-qubits
$[[7,1,3]]$.

%  ___ ___ __ __ __ _ ___ 
% (_-</ -_) _/ _/ _` / _ \
% /__/\___\__\__\__,_\___/
%             )_)         
\section{Código de repetição}\label{sec:cod3}

Se queremos proteger um bit clássico contra \textit{bit flip} a técnica mais
simples que podemos usar é repeti-lo em três bits, assim, se um bit diferir dos
demais sabemos que houve um \textit{bit flip} no mesmo. Desta forma, codificamos o bit
0 em 000 e 1 em 111, e se em algum momento estivermos em um estado diferente
de 000 ou 111, como por exemplo, 101 saberemos que aconteceu um \textit{Bit
Flip} em algum bit. Podemos extrapolar esse conceito para qubits, codificando
o estado $\ket{0}$ em $\ket{000}$ e $\ket{1}$ em $\ket{111}$. Assim, se o qubit
não estiver em algum desses estados ou em uma superposição dele sabemos que
houve a atuação de um canal de \textit{bit flip}.

O código de \textit{bit flip} é composto pelas palavras
\begin{align}
  &\begin{aligned}
    \ket{0}_L = \ket{000}
  \end{aligned}\\
  &\begin{aligned}
    \ket{1}_L = \ket{111},
  \end{aligned}
\end{align}
chamadas de estados lógicos do qubit, e pela superposição de $\ket{0}_L$ e
$\ket{1}_L$, sendo que, qualquer palavra não pertencente ao código, é um erro.

Podemos codificar um qubit $\ket{\psi} = \alpha\ket{0} + \beta\ket{1}$ no
código de \textit{bit flip} conforme o circuito da Figura~\ref{fig:3cod}, pelos
seguintes passo:
\begin{align}
  \phantom{[cnot_{0,2}][cnot_{0,1}]} 
  &\begin{aligned}
    \mathllap{\ket{\psi}\ket{00}} = \alpha\ket{000} + \beta\ket{100}
  \end{aligned}\\
  &\begin{aligned}
    \mathllap{[cnot_{0,1}]\ket{\psi}\ket{00}} = \alpha\ket{000} + \beta\ket{110}
  \end{aligned}\\
  &\begin{aligned}
    \mathllap{[cnot_{0,2}][cnot_{0,1}]\ket{\psi}\ket{00}} = \alpha\ket{000} +
    \beta\ket{111} = \ket{\psi}_L
  \end{aligned}
\end{align}

\begin{figure}[H]
   \centering
       \includegraphics[width=.2\textwidth]{qcirc/3bit.pdf} 
  \caption{Preparação do código repetição para \textit{bit flip}.}\label{fig:3cod}
\end{figure}

Para detectar e corrigir um erro adicionamos dois qubits ancilares, como na
Figura~\ref{fig:3corect}, e os medimos colapsando o sistema conforme a
Tabela~\ref{tab:sinm}. Chamamos os resultados das medidas das ancilas de
síndrome do erro e é com ela que conseguimos identificar o erro para podermos
corrigi-lo, a Tabela~\ref{tab:3sin} apresenta as correções necessárias para
cada medida de síndrome.

\begin{figure}[H]
  \centering
  \includegraphics[width=.5\textwidth]{qcirc/3bitcor.pdf} 
  \caption{Código de correção de \textit{bit flip}.}\label{fig:3corect}
\end{figure}

\begin{table}[H]
  \centering
  \begin{tabular}{cc}
    \toprule
     Medida & Estado\\
    \midrule
    00 &  $\alpha\ket{000}\ket{00}+\beta\ket{111}\ket{00}$\\
    01 &  $\alpha\ket{001}\ket{01}+\beta\ket{110}\ket{01}$\\
    10 &  $\alpha\ket{010}\ket{10}+\beta\ket{101}\ket{10}$\\
    11 &  $\alpha\ket{100}\ket{10}+\beta\ket{011}\ket{11}$\\
    \bottomrule
  \end{tabular}
  \caption{Medida da síndrome para o código de \textit{bit flip}}\label{tab:sinm}
\end{table}

\begin{table}[H]
  \centering
  \begin{tabular}{lcc}
    \toprule
     Erro & Síndrome &Correção\\
    \midrule
     Sem erro &00 & \\
     qubit 2 & 01 &$X_2$\\
     qubit 1 & 10 &$X_1$\\
     qubit 0 & 11 &$X_0$\\
    \bottomrule
  \end{tabular}
  \caption{Medida da síndrome e correção para o código de \textit{Bit
  Flip}}\label{tab:3sin}
\end{table}

Para exemplificar o protocolo de correção, imagine que temos um qubit no estado
\begin{equation}
  \ket{\psi}_L = \alpha\ket{0}_L+\beta\ket{1}_L 
\end{equation}
que sofreu um \textit{bit flip} no segundo qubit, passando para o estado
\begin{equation}
  \ket{\psi}_E = \alpha\ket{010}+\beta\ket{101}.
\end{equation}
Para fazer a identificação e correção adicionamos duas ancilas no estado
$\ket{00}$
\begin{equation}
  \ket{\psi}_E\ket{0_a0_b} = \alpha\ket{010}\ket{0_a0_b}+\beta\ket{101}\ket{0_a0_b}
\end{equation}
e aplicamos as $cnot$s conforme a Figura~\ref{fig:3corect}
\begin{align}
  [cnot_{0, a}]\ket{\psi}_E\ket{0_a0_b} =
  \alpha\ket{010}\ket{0_a0_b}+\beta\ket{101}\ket{1_a0_b}\\
  [cnot_{1, a}][cnot_{0, a}]\ket{\psi}_E\ket{0_a0_b} =
  \alpha\ket{010}\ket{1_a0_b}+\beta\ket{101}\ket{1_a0_b}\\
  [cnot_{0, b}][cnot_{1, a}][cnot_{0, a}]\ket{\psi}_E\ket{0_a0_b} =
  \alpha\ket{010}\ket{1_a0_b}+\beta\ket{101}\ket{1_a1_b}\\
  [cnot_{2, b}][cnot_{0, b}][cnot_{1, a}][cnot_{0, a}]\ket{\psi}_E\ket{0_a0_b}
  = \alpha\ket{010}\ket{1_a0_b}+\beta\ket{101}\ket{1_a0_b},
\end{align}
em seguida, medimos os qubits ancilares, que resultam em 10 e colapsam o
sistema em
\begin{equation}
  \alpha\ket{010}+\beta\ket{101},
\end{equation}
e com os resultados das medidas aplicamos a correção conforme a Tabela~\ref{tab:3sin},
voltando para o estado sem erro
\begin{align}
  X_1(\alpha\ket{010} + \beta\ket{101}) & = \alpha\ket{000} +\beta\ket{111} \\
                                        & = \alpha\ket{0}_L + \beta\ket{1}_L.
\end{align}

O código de \textit{bit flip} é $[[3,1, d = 3]]$, onde $d$ é a distância entre
as palavras $\ket{0}_L$ e $\ket{1}_L$, isso significa que é necessário alterar
3 qubits para levar $\ket{0}_L$ em $\ket{1}_L$ e vice versa. Outra
característica é que esse código só corrige ${d-1\over2} = 1$ erro. Mais do
que um erro o código é incapaz de corrigir. Se acontecem dois \textit{bit flip}
em qubits distintos no estado $\ket{0}_L$, esse estado passa a ficar mais perto
do estado $\ket{1}_L$ do que em $\ket{0}_L$. Assim, o código irá corrigir
erroneamente para o estado $\ket{1}_L$. No caso de acontecer um \textit{Bit
Flip} em cada qubit o código não será capaz de identificar o erro. Exemplos 
podem ser vistos na Tabela~\ref{tab:erroerro}.

\begin{table}[H]
   \centering
   \begin{tabular}{cccl}
     \toprule
     \makecell{\textit{bit flip}\\nos qubits} & Estado com erro &
     \makecell{Resultado\\da correção }& Comentário\\
     \midrule
     0 & $\alpha\ket{100} + \beta\ket{011}$ & $\alpha\ket{0}_L +
     \beta\ket{1}_L$ & Erro identificado e corrigido.\\
     1 & $\alpha\ket{010} + \beta\ket{101}$ & $\alpha\ket{0}_L +
     \beta\ket{1}_L$ & Erro identificado e corrigido.\\
     2 & $\alpha\ket{001} + \beta\ket{110}$ & $\alpha\ket{0}_L +
     \beta\ket{1}_L$ & Erro identificado e corrigido.\\
      0 e 1 & $\alpha\ket{110} + \beta\ket{001}$ & $\alpha\ket{1}_L +
     \beta\ket{0}_L$ & Erro incorretamente identificado.\\
     0 e 2 & $\alpha\ket{101} + \beta\ket{010}$ & $\alpha\ket{1}_L +
     \beta\ket{0}_L$ & Erro incorretamente identificado.\\
     1 e 2 & $\alpha\ket{011} + \beta\ket{100}$ & $\alpha\ket{1}_L +
     \beta\ket{0}_L$ & Erro incorretamente identificado.\\
     0,1 e 2 & $\alpha\ket{111} + \beta\ket{000}$ & & Presença de erro não identificado\\
     \bottomrule
   \end{tabular}
   \caption{Erros e correções para o estado $\alpha\ket{0}_L + \beta\ket{1}_L$
   codificado no código de \textit{bit flip}.}\label{tab:erroerro}
\end{table}

Podemos utilizar o mesmo conceito para construir um código tolerante a um
\textit{phase flip}, sendo as palavras deste código\begin{align}
  \ket{0}_L = {\ket{000}+\ket{111}\over\sqrt{2}} = \ket{+++} \\
  \ket{1}_L = {\ket{000}-\ket{111}\over\sqrt{2}} = \ket{---}.
\end{align}
Utilizando a base $\{\ket{+},\ket{-}\}$ visto na equação~\ref{eq:maismenos}, um
\textit{phase flip} leva um qubit do estado $\ket{+}$ para o estado $\ket{-}$ e
vice versa.

Por exemplo, se acontecer um \textit{phase flip} no primeiro qubit do estado
$\ket{+++}$, ele passa a estar no estado $\ket{-++}$. Assim, para corrigir esse
erro passamos todos os qubits por uma porta de Hadamard $HHH\ket{-++} =
\ket{100}$, e utilizamos o mesmo protocolo visto para o código de \textit{Bit
Flip} para corrigir o erro. Todo protocolo de codificação e correção de erro do
código de \textit{phase flip} pode ser visto no circuito de
Figura~\ref{fig:3phasecorect}.
%\begin{figure}[H]
%  \centering
%  \includegraphics[width=.25\textwidth]{qcirc/3phase.pdf} 
%  \caption{Preparação do código repetição para \textit{phase flip}.}\label{fig:3phase}
%\end{figure}

\begin{figure}[H]
   \centering
       \includegraphics[width=.6\textwidth]{qcirc/3phasecor.pdf} 
  \caption{Código de correção de \textit{phase flip}.}\label{fig:3phasecorect}
\end{figure}

%  ___ ___ __ __ __ _ ___ 
% (_-</ -_) _/ _/ _` / _ \
% /__/\___\__\__\__,_\___/
%             )_)         
\section{Código de Shor}\label{sec:codshor}

O código de Shor concatena 3 códigos de \textit{bit flip} a um código de
\textit{phase flip} para construir um código tolerante a dois erros, um
\textit{bit flip} e um \textit{phase flip}. Um qubit $\ket{\psi}_L$ pode ser
preparado no código de Shor pelo circuito da Figura~\ref{fig:shor}.

O código de Shor possui as seguintes palavras:
\begin{align}
  \ket{0}_L =
  {(\ket{000}+\ket{111})(\ket{000}+\ket{111})(\ket{000}+\ket{111})\over2\sqrt{2}}\\
  \ket{1}_L =
  {(\ket{000}-\ket{111})(\ket{000}-\ket{111})(\ket{000}-\ket{111})\over2\sqrt{2}}
\end{align}

\begin{figure}[H]
   \centering
       \includegraphics[width=.45\textwidth]{qcirc/shorflip.pdf} 
  \caption{Preparação do código de Shor.}\label{fig:shor}
\end{figure}

Para preparar o qubit $\ket{\psi} = \alpha\ket{0} + \beta\ket{1}$ no código de
Shor primeiramente preparamos ele no código de \textit{phase flip}
\begin{equation}
  \ket{\psi} = \alpha\ket{0} +\beta\ket{1}\rightarrow \alpha\left({\ket{000}+\ket{111}\over\sqrt{2}}\right) +\beta\left({\ket{000}-\ket{111}\over\sqrt{2}}\right)
\end{equation}
Em seguida preparamos cada qubit com o código de \textit{bit flip}
\begin{equation}
  \begin{aligned}
    \ket{\psi}_L = &
 \alpha\left[{(\ket{000}+\ket{111})(\ket{000}+\ket{111})(\ket{000}+\ket{111})\over2\sqrt{2}}\right]\\
  &+\beta\left[{(\ket{000}-\ket{111})(\ket{000}-\ket{111})(\ket{000}-\ket{111})\over2\sqrt{2}}\right]
\end{aligned}
\end{equation}

Para identificar e corrigir os erros, inicialmente corrigimos a parte do código
referente ao \textit{bit flip}, como visto na Figura~\ref{fig:3corect}, e, por
fim, corrigimos \textit{phase flip}, como visto na
Figura~\ref{fig:3phasecorect}.

%  ___ ___ __ __ __ _ ___ 
% (_-</ -_) _/ _/ _` / _ \
% /__/\___\__\__\__,_\___/
%             )_)         
\section{Códigos estabilizadores}\label{sec:estabilizador}

Os códigos estabilizadores são uma importante classe de códigos de correção de
erro, pois eles trazem um formalismo que ajuda na generalização da correção de erros
e na construção de circuitos. O código de Steane $[[7,1,3]]$ será usado como
exemplo para apresentar a classe dos códigos estabilizadores, sendo os exemplos
apresentados facilmente aplicados para outros códigos estabilizadores.

Os códigos estabilizadores são descritos por um conjunto gerador composto de
operadores que formam um grupo fechado na multiplicação\footnote{Um conjunto
$\mathcal{G}$ é fechado sobre uma operação $*$ se e somente se $a*b=c$ $\forall
a,b,c \in \mathcal{G}$.} que estabiliza todas as palavras do código. Um
estado $\ket{\psi}$ é estabilizado por um operador $K$ se e somente se
$\ket{\psi}$ for autovetor de $K$ com autovalor +1, ou seja
\begin{equation}
  K\ket{\psi} = \ket{\psi}.
\end{equation}

Como exemplo, o código de \textit{bit flip} é estabilizado pelo grupo de
operadores $\mathcal{G}=\{ZZI, ZIZ, IZZ, III\}$. Pode ser facilmente verificado
que o $\mathcal{G}$ é fechado sobre a multiplicação e que toda palavra do
código de \textit{bit flip} é autovetor com autovalor +1 dos operadores de
$\mathcal{G}$. Podemos definir $\mathcal{G}$ por seu conjunto gerador, ou seja,
por um subconjunto que consiga gerar todos os elementos de $\mathcal{G}$ a
partir da multiplicação. Como por exemplo, o grupo $\mathcal{G}$ pode ser gerado
pelo conjunto gerador $\braket{IZZ, ZIZ}$, pois
\begin{align}
  ZZI & = IZZ \times ZIZ\\
  III & = IZZ \times IZZ.
\end{align}

Um código estabilizador é totalmente descrito pelos seus estabilizadores,
assim, os seguintes códigos podem ser descritos como:
\begin{itemize}
  \item Código de \textit{bit flip} $[[3,1,3]]$
    \begin{align}
      K^1 &= IZZ &
      K^2 &= ZIZ.
    \end{align}
  \item Código de \textit{phase flip} $[[3,1,3]]$
    \begin{align}
      K^1 &= IXX &
      K^2 &= XIX.
    \end{align}
  \item Código de Shor $[[9,1,3]]$
    \begin{align}
      K^1 &= Z_0 Z_1 &
      K^2 &= Z_0 Z_2 &
      K^3 &= Z_3 Z_4\\ 
      K^4 &= Z_3 Z_5 &
      K^5 &= Z_6 Z_7 &
      K^6 &= Z_6 Z_9\\ 
      K^7 &= X_0 X_1 X_2 X_3 X_4 X_5 &
      K^8 &= X_0 X_1 X_2 X_6 X_7 X_8
    \end{align}
  \item Código de Steane (7-qubits) $[[7,1,3]]$
    \begin{align}
  \label{eq:7code123}
  K^1 &= IIIXXXX & K^2 &= XIXIXIX & K^3 &= IXXIIXX\\
  K^4 &= IIIZZZZ & K^5 &= ZIZIZIZ & K^6 &= IZZIIZZ
    \end{align}
  \item Código de 5-qubits $[[5,1,3]]$
    \begin{align}
      K^1 &= IZXXZI &
      K^2 &= ZIZXXI\\
      K^3 &= XZIZXI &
      K^4 &= XXZIZI
    \end{align}
\end{itemize}
O código de 5-qubits não será detalhado aqui. Para mais informações veja a
referência \citeonline{5code}.

\subsection{Preparação do estado}

Para preparar um conjunto de qubits em um qubit lógico, precisamos projetá-los
como autovetor com autovalor $+1$ de todos os estabilizadores do código. 

\begin{figure}[H]
  \centering
  \includegraphics[width=.45\textwidth]{qcirc/sindrome.pdf} 
  \caption{Prepara $\ket{\psi}$ como autovetor $\pm1$ de
  $K$.}\label{fig:sprep}
\end{figure}

Para preparar o qubit $\ket{\psi}$, primeiramente, é necessário projetá-lo como
autovalor $\pm1$ de todos estabilizadores $K^i$ do código. Como na
Figura~\ref{fig:sprep}, adicionamos uma ancila no estado $\ket{0}$ e passamos
ela por uma porta Hadamard, em seguida, é aplicada a porta controlada $K^i$
deixando o sistema no estado 
\begin{equation}
  \ket{\psi}\ket{0} + K^i\ket{\psi}\ket{1}\over\sqrt{2}.
\end{equation}
Como $\ket{\psi}$ só pode ser autovalor $\pm1$ de $K^i$, após o segundo
Hadamard temos 
\begin{equation}
  \ket{\psi}\ket{0} + K^i\ket{\psi}\ket{0}
\end{equation}
se $\ket{\psi}$ for autovalor $+1$ de $K^i$, e
\begin{equation}
  \ket{\psi}\ket{1} - K^i\ket{\psi}\ket{1}
\end{equation}
se $\ket{\psi}$ for autovalor $-1$ de $K^i$. É feita a medida da ancila e, após
passar por todos os $K^i$ estabilizadores, usamos os resultados das medidas para
transformar o estado de autovalor $\pm1$ para autovalor $+1$ de todos $K^i$.

Como por exemplo, para preparar um qubit no estado $\ket{0}_L$ do código Steane
é necessário, inicialmente que os qubits estejam no estado $\ket{0}^{\otimes7}$,
então são adicionadas 3 ancilas no estado $\ket{000}$, que passam cada uma por
uma porta de Hadamard, para, logo em seguida, serem aplicadas às portas $K^1$,$K^2$
e $K^3$ (equação~\ref{eq:7code123}) controladas. Logo após, novamente cada ancila 
passa por uma porta de Hadamard, e, ao final, elas medidas, e, o resultado da medida
é usado para aplicar uma porta $Z_i$, dependente do resultado da medida, que
projeta o estado em um autovetor com autovalor $+1$ de todos os estabilizadores
do código de Steane. Esse processo pode ser visto na Figura~\ref{fig:7prep}.

\begin{figure}[H]
  \centering
  \includegraphics[width=.5\textwidth]{qcirc/7prep.pdf} 
  \caption{Circuito para preparar $\ket{0}^{\otimes7}$ em $\ket{0}_L$ do código
  de 7-qubits, após a medida das ancilas é aplicado uma porta $Z$ no qubit $i =
  4M_1+M_2+2M_3-1$.}\label{fig:7prep}
\end{figure}

\subsection{Medida de síndrome}

Os estabilizadores comutam com as operações lógicas e anti comutam com os erros,
ou seja, sendo $U$ uma operação lógica que mapeia um estado do código a outro  
\begin{align}
  K^i U = UK^i, \quad \forall K^i,
\end{align}
e, sendo $E$ um erro que leva um estado pertencente ao código a um estado 
fora do código 
\begin{align}
  K^i E = -EK^i.
\end{align}
Com isso, podemos usar o mesmo circuito da Figura~\ref{fig:7prep} para fazer a
medida da síndrome. Para isso, considere $\ket{\psi}_L$ como sendo uma palavra
do código e $E$ como sendo um possível erro. Assim, para fazer a medida da
síndrome é adicionada uma ancila no estado $\ket{0}$
\begin{equation}
    E\ket{\psi}_L\ket{0},
\end{equation}
em seguida, a ancila é passada por uma porta de Hadamard
\begin{equation}
  {E\ket{\psi}\ket{0} + E\ket{\psi}\ket{1}\over\sqrt{2}},
\end{equation}
logo em seguida, é aplicada a operação controlada com o estabilizador $K^i$
tendo a ancila como qubit de controle
\begin{equation}
  {E\ket{\psi}\ket{0} + K^i E\ket{\psi}\ket{1}\over\sqrt{2}},
\end{equation}
podemos reescrever o estado como 
\begin{equation}
  {E\ket{\psi}\ket{0} + {(-1)}^m EK^i\ket{\psi}\ket{1}\over\sqrt{2}} =
  {E\ket{\psi}\ket{0} + {(-1)}^m E\ket{\psi}\ket{1}\over\sqrt{2}},
\end{equation}
dado que o erro anti comuta com o operador $K^i$, $m$ é  igual a 0 se $E$ não
for um erro e 1 se for um erro, assim, após passar a ancila novamente pela
porta de Hadamard temos 
\begin{align}
  &\begin{aligned}
  E\ket{\psi}\ket{0} \text{~se $E$ não for um erro, e}
  \end{aligned}\\
  &\begin{aligned}
  E\ket{\psi}\ket{1} \text{~se $E$ for um erro}.
  \end{aligned}
\end{align}
Assim, com a medida das ancilas o estado colapsa em um estado onde
aconteceu ou não aconteceu o erro. Esse processo é feito para todos os
estabilizadores a fim de identificar e corrigir os erros. 

A correção do erro para o código de Steane é dividida em duas partes, a
primeira, usa o mesmo circuito da Figura~\ref{fig:7prep} para corrigir
\textit{phase flip} e, a segunda, usa um circuito semelhante a primeira parte,
apenas trocando os estabilizadores que são usados para corrigir \textit{Bit
Flip}. A Figura~\ref{fig:7corre} traz todo o circuito para correção de erro do
código de 7-qubits.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{qcirc/7corre.pdf} 
  \caption{A medida de síndrome para código de Steane é dividida em duas partes,
  onde, a primeira, identifica e corrige \textit{phase flip} aplicando uma porta
  $Z$ no qubit  $i = 4M_1+M_2+2M_3-1$ e, a segunda, identifica e corrige
  \textit{bit flip} aplicando uma porta $X$ no quibit $i =
  4M_4+M_5+2M_6-1$.}\label{fig:7corre}
\end{figure}


