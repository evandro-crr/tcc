\chapter*[Introdução]{Introdução}
\addcontentsline{toc}{chapter}{Introdução}

Como apresentado por \citeauthor{hennessy}, dos anos 90 até o início dos anos
2000, diversos fatores eram favoráveis para que a taxa de crescimento do poder
computacional fosse de 52\% ao ano (dobrando, aproximadamente, a cada dois anos).
Porém, em 2003, devido a desaceleração da lei de Moore\cite{moore}, ao fim da
escalabilidade de Dennard\cite{dennard} e, ao limite dos processadores
multinúcleo, causado pela lei de Amdahl\cite{amdahl}, a taxa de crescimento do
poder computacional caiu para 23\% ao ano (dobrando, aproximadamente, a cada
quatro anos). Contudo, essa taxa continuou caindo, sendo que a prevista
para 2018 (ano seguinte a publicação do livro
\textit{Computer Architecture: A Quantitative Approach}\cite{hennessy}) foi
de 3,5\% (dobrando, aproximadamente, a cada vinte e oito anos e meio). Todavia, com
o advento dos primeiros computadores quânticos comerciais, a indústria e a
academia veem a computação quântica como a porta de entrada para uma nova era
de crescimento exponencial do poder computacional.

A possibilidade de utilizar um computador quântico foi inicialmente apresentada
por \citeauthor{feynman} em \citeyear{feynman}, quando ele propôs que um
computador quântico poderia simular eficientemente um sistema quântico, uma
tarefa difícil para os computadores clássicos (até mesmo para os atuais). Esse,
foi um dos primeiros indícios de que um computador quântico pode ter ganho
sobre um computador clássico para resolução de alguns problemas. Contudo, a
primeira demonstração disto foi feita por \citeauthor{shor94} em
\citeyear{shor94}, quando ele apresentou dois algoritmos quânticos para
resolver dois problemas $\mathcal{NP}$ (o logaritmo discreto e a fatoração) que
funcionam em tempo polinomial em um computador quântico. Atualmente, já foram
propostos vários algoritmos quânticos mais eficientes que seus respectivos
algoritmos clássicos, porém, ainda há muito a ser desenvolvido nessa área.

O simulador QSystem foi desenvolvido para auxiliar no estudo de algoritmos,
protocolos e códigos quânticos. Por ser baseado no modelo de computação
circuital, um circuito quântico é facilmente transcrito para o simulador. Essa
característica faz com que o simulador abstraia para o usuário grande parte da
álgebra linear envolvida na computação quântica. Construído para operar tanto em
vetor de estado quanto em matriz densidade, o simulador possibilita a simulação
de erros quânticos, fazendo que o mesmo possa ser usado no estudo de códigos de
identificação e correção de erros quânticos.

Para obter um bom desempenho, o simulador, foi desenvolvido em C++ utilizando a
biblioteca de álgebra linear Armadillo\cite{arma} e a ferramenta
SWIG\cite{swig}, utilizada para gerar a interface entre o código escrito em
C++17 e o interpretador do Python 3. Assim, o simulador QSystem é distribuído
como um módulo de Python.

Este trabalho é dividido em duas partes, onde inicialmente, na Parte
\ref{par:um}, é apresentado uma revisão teórica sobre os temas da computação
quântica pertinentes para o compreendimento das funcionalidades e da
implementação do simulador quântico, posteriormente, na Parte \ref{par:dois},
as funcionalidades do simulador são apresentadas juntamente a exemplos de uso e
uma documentação sobre suas implementações.

A Parte \ref{par:um} é composta por cinco capítulos, onde são apresentados os
seguintes temas: representação e características de um bit quântico ou qubit
(Capítulo \ref{ch:qbit}); os quatro postulados da mecânica quântica segundo
o livro \textit{Quantum computation and quantum information}\cite{nielsen} 
(Capítulo \ref{ch:postulados}); o modelo de circuitos
quânticos que inspira o simulador (Capítulo \ref{ch:circuito}); uma
representação alternativa do estado quântico, denominada matriz densidade,
usada para representar erros quânticos (Capítulo  \ref{ch:matriz}); e, como
representar um erro quântico e os erros quânticos clássicos (Capítulo
\ref{ch:erro}).

A Parte \ref{par:dois} é composta por oito capítulos, onde são abordados os
seguintes aspectos do simulador: uma visão geral sobre o simulador QSystem
(Capítulo \ref{ch:qsys1}); como é feita a evolução do estado quântico e criação
de portas lógicas quânticas (Capítulo \ref{ch:evollg}); como os qubits são
medidos (Capítulo \ref{ch:mea1}); a implementação dos canais de erros quânticos
no simulador (Capítulo \ref{ch:erroimp}); as operações de adicionar e remover
qubits ancilares (Capítulo \ref{ch:ancich}); outras funcionalidades do
simulador, como mudança de representação do estado quântico, manipulação da
matriz/vetor que representa o estado e, salvar e carregar estados e portas
lógicas quânticas (Capítulo \ref{ch:outros}); como é criada a interface entre o
código C++ e o interpretador do Python, além de como é montado o pacote Python
para instalação do módulo QSystem (Capítulo \ref{ch:montagem}); e, por fim, são
apresentados dois exemplos de implementação utilizando o simulador (Capítulo
\ref{ch:exqys}).

Ao final do trabalho é apresentado a conclusão e os possíveis trabalhos
futuros, além de alguns apêndices que complementam o texto principal.


%A computação quântica é capaz, através da utilização de propriedades da
%mecânica quântica\cite{nielsen}, como superposição e entrelaçamento, de
%resolver problemas $\mathcal{NP}$ em tempo polinomial, como por exemplo a
%fatoração, um problema $\mathcal{NP}$, que pode ser feita em tempo polinomial
%em um computador quântico\cite{shor99}, e devido aos avanços na construção de
%computadores quânticos as pesquisas sobre algoritmos quânticos ganhão cada vez
%mais relevância.
%
%Com o intuito de facilitar futuras pesquisas na área de algoritmos quânticos
%esse trabalho desenvolve um simulador de computação quântica para Python,
%denominado QSystem. Para obter um bom desempenho o simulador foi desenvolvido
%em \texttt{C++} utilizara a biblioteca de álgebra linear Armadillo, e a
%ferramenta SWIG para fazer a interface entre \texttt{C++} e Python.
%O simulador foi desenvolvido para operar tanto em vetor quanto em matriz
%densidade, sendo o estado em vetor recomendado para estudos de algoritmos
%quânticos em geral e a matriz densidade recomendado para o estudo de
%identificação e correção de erros quânticos.
%
%O trabalho e dividido em duas partes, sendo a primeira parte uma revisão
%teórica sobre computação quântica e a segunda parte a documentação do simulador
%QSystem. A primeira parte tem o objetivo de apresentar os conceitos de
%computação quântica utilizados para desenvolver o simulador, dividida em cinco
%capítulos, sendo eles: Capítulo \ref{ch:qbit} Bit quântico, conceituação do que
%é um qubit; Capítulo~\ref{ch:postulados} Postulados da mecânica quântica,
%apresentação dos postulados que regem a mecânica quântica; Capítulo
%\ref{ch:circuito} Circuitos quântico, apresenta o modelo de computação quântica
%na qual o QSystem se baseá; Capítulo \ref{ch:matriz} Matriz densidade, descreve
%uma outra forma de representar qubits, também implementado no simulador,
%recomendado para o estudo de identificação e correção de erros quânticos;
%Capítulo \ref{ch:erro} Canais de erro, apresenta um modelo para descrever erros
%quânticos. A segunda parte consiste na documentação do desenvolvimento do
%simulador, que na sua maior parte é a aplicação do que foi visto na revisão
%teórica, dividido em seis capítulos, sendo eles:...



%A computação quântica é capaz, através da utilização de propriedades da
%mecânica quântica\cite{nielsen}, como superposição e entrelaçamento, de
%resolver problemas $\mathcal{NP}$ em tempo polinomial, como por exemplo a
%fatoração em números primos, um problema $\mathcal{NP}$, que pode ser feita em
%tempo polinomial em um computador quântico\cite{shor99}. Entretanto, para que
%isso seja possível, é necessário a utilização de um computador quântico. Porém,
%as dificuldades de construção de um, devido aos erros introduzidos pela
%interação do sistema
%quântico com outros sistemas, fazem com que atualmente os computadores
%quânticos possuam uma taxa de erro não nula, e isto implica em uma computação
%imprecisa. Para mitigar isto, um mecanismo de correção de erros é necessário.
%
%Na computação clássica\footnote{O termo computação clássica é utilizado para
%diferenciar a computação não quântica da quântica} é possível fazer correção de
%erros através das seguintes formas\cite{shoredx}: \textbf{checkpoints}, que
%consiste em salvar o estado do sistema em determinados pontos e, quando um erro
%é detectado, o sistema é restaurado para o último estado salvo;
%\textbf{redundância}, consiste em fazer cópia do sistema e descartar as que,
%após a computação, divirjam das demais; e utilizando \textbf{código de
%correção}, adicionando informação extra ao sistema de modo que seja possível
%verificar e corrigir determinados erros.
%
%Na computação quântica, devido ao teorema de não clonagem\cite{wootters82} da
%mecânica quântica que diz que é impossível copiar um estado quântico, o único
%mecanismo viável para fazer correção de erros é utilizando \textbf{códigos de
%correção de erros}\cite{devitt13}.
%
%Assim como na computação clássica onde temos o bit como unidade básica de
%computação, na computação quântica temos o qubit como unidade básica de
%computação. Portanto um código de correção de erros quântico consiste em
%agrupar alguns qubits físico para formar um qubit lógico, para caso aconteça um
%erro em algum qubit físico ser possível corrigir esse erro com o auxílio dos
%outros qubits não mudando o valor do qubit lógico. Uma das premissas dos
%códigos de correção de erros é que não aconteçam erros simultâneos, sendo assim
%são incapazes de identificar correlações entre os erros quânticos. 
%
%Para identificar correlações entre erros quânticos, propõe-se treinar uma rede
%neural onde os dados para treinamento serão extraídos de um simulador de um
%computador quântico que será construído de forma que possa gerar erros de
%maneira controlada, assim, poderemos extrair dados através da execução de
%alguma computação no simulador, forçando a ocorrência de erro e, coletar os
%dados dos códigos de correção, gerando, assim, um banco de dados consistente
%para o treinamento da rede neural.
%
%O foco do trabalho é apenas identificar a existência de correlação entre erros
%quânticos, não tendo nenhum intuito de identificar ou corrigir a fonte dos
%erros.

