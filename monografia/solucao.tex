\renewcommand\listingscaption{Exemplo de código}
\chapter{QSystem: Simulador quântico para Python}

O simulador QSystem foi desenvolvido para o estudo de identificação e correção
de erros em um computador quântico, operando tento em estado puro, com
representação vectorial, quanto em estado misto com representação em matriz
densidade, como visto no Capitulo~\ref{ch:matriz}. Os erros de inversão de Bit
e fase, canal de despolarização e decaimento de amplitude são todos simulados
conforme o formalismo apresentado no Capitulo~\ref{ch:erro}. A possibilidade de
operar com estado misto e de simular os erros quânticos tradicionais
(Secção~\ref{sec:erro}) dão ao simulador as condições necessárias para usa-lo
para trabalhar com identificação e correção de erros, alem disso, por também
operar em estado puto, o simulador pode ser usado para o estudo de protocolos e
algoritmos quânticos no geral.

O simulador escrito em \mintinline{cpp}{C++} é usado como uma biblioteca
\mintinline{python}{Python} e seu funcionamento é descrito nesse capitulo
(detalhes sobre a implementação em \mintinline{python}{C++} são dadas no
Capitulo~\ref{ch:imple}). Inicialmente são apresentadas as interfaces das
classes \mintinline{python}{QSystem} e \mintinline{python}{Gate}
(Secção~\ref{sec:classes}), em seguida de maneira mais detalhada são descritos
os métodos relacionados com: a evolução do sistema, e as portas lógicas
envolvidas (Secção~\ref{sec:qsysporta}); a medida do sistema
(Secção~\ref{sec:qsysmedida}); os canais de erros (Secção~\ref{sec:qsyserro});
os qubits ancilares (Secção~\ref{sec:qsysancila}). Por fim são apresentados
exemplos da utilização do simulador (Secção~\ref{sec:qsysexemplos}). 

\section{Biblioteca QSystem}\label{sec:classes}

A biblioteca QSystem é composta de apenas duas classes, a \mintinline{python}{QSystem} que
implementa o computador quântico com todas suas dinâmicas de evolução e medida e
a classe \mintinline{python}{Gate} que armazena as portas lógicas usadas na computação,
ambas são detalhadas nas subsecções seguintes.

\subsection{Classe \texttt{QSystem}}\label{sub:qsysmetodos}

A classe \mintinline{python}{QSystem} armazena o estado do sistema, seja ele em
estado puro, com representação vetorial, ou em estado misto\footnote{Nem toda
matriz densidade representa um estado misto, porem por simplificação considere
que, no contexto do simulador, estado misto é sinônimo de matriz densidade.}
com representação em matriz densidade. Durante a instanciação é configurado a
quantidade de qubits que o sistema possui, como não há nenhuma limitação na
maneira em que os qubits se conectam, diferente de um computador quântico real,
todos os qubits podem interagir diretamente entre si. Para ajudar na
interpretação, considera que os qubits são armazenados em sequencia e são
identificados a partir de um índice que varia de 0 à $n{-}1$, onde $n$ o número
de qubtis, sendo 0 o qubit mais a esquerda e $n{-}1$ o qubit mais a direita.

Os métodos da classe \mintinline{python}{QSystem} são listados abaixo com uma
breve descrição do seu funcionamento, retorno e parâmetros.

\begin{description}
  \item \begin{minted}{python}
def __init__(self, nqbits, seed, gate, state)
  \end{minted}
  Construtor, inicia todos os qubits no estado $\ket{0}$. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{nqbits}: número de qubits do sistema; 
    \item \mintinline{python}{seed}: número utilizado para iniciar o gerador de
      números aleatórios;
    \item \mintinline{python}{gate}: instancia da classe \mintinline{python}{Gate};
    \item \mintinline{python}{state}: \mintinline{python}{str} com o valor
      \mintinline{python}{'mix'} ou \mintinline{python}{'pure'} que inicia o
      sistema em, respectivamente, estado misto ou estado puro. 
  \end{itemize}

  \item \begin{minted}{python}
def evol(self, gate, qbit)
  \end{minted}
  Aplica uma porta lógica quântica em um qubit. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{gate}: \mintinline{python}{str} de um caractere indicando a porta lógica que
      será apicada;
    \item \mintinline{python}{qbit}: índice do qubit no qual a porta será aplicada.
  \end{itemize}


  \item \begin{minted}{python}
def evol (self, gate, qbegin, qend)
  \end{minted}
  Aplica uma porta lógica quântica em uma sequencia de qubits. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{gate}: \mintinline{python}{str} de um caractere indicando a porta lógica que
      será apicada;
    \item \mintinline{python}{qbegin}: índice do primeiro qubit no qual a porta
      será aplicada;
    \item \mintinline{python}{qend}: índice+1 do ultimo qubit, da sequencia, no qual a
      porta será aplicada.
  \end{itemize}

  \item \begin{minted}{python}
def evol(self, gates)
  \end{minted}
  Aplica uma porta lógica a cada qubit do sistema. Parâmetro:
  \begin{itemize}
    \item \mintinline{python}{gates}: \mintinline{python}{str} de tamanho
      referente ao número total de qubits no sistema, incluindo ancilas, onde
      cada caractere diz respeito a aplicação de uma porta lógica no qubit de
      respectiva posição.
  \end{itemize}
 
  \item \begin{minted}{python}
def cnot(self, target, control)
  \end{minted}
  Aplica uma porta $cnot$. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{target}: índice do qubit alvo;
    \item \mintinline{python}{control}: lista com índices dos qubits de controle.
  \end{itemize}

  \item \begin{minted}{python}
def cevol(self, u)
  \end{minted}
  Aplica uma porta lógica controlada. Parâmetro:
  \begin{itemize}
    \item \mintinline{python}{u}: \mintinline{python}{str} referente a porta
      lógica controlada que será aplicada no sistema.
  \end{itemize}

  \item \begin{minted}{python}
def cevol(self, u, aux_gate)
  \end{minted}
  Aplica uma porta lógica controlada. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{u}: \mintinline{python}{str} referente a porta
      lógica controlada que será aplicada no sistema.
    \item \mintinline{python}{aux_gate}: instancia de classe
      \mintinline{python}{Gate} que armazena a operação que será aplicada.
  \end{itemize}

  \item \begin{minted}{python}
def measure(self, qbit)
  \end{minted}
  Mede um qubit na base computacional. Parâmetro:
  \begin{itemize}
    \item \mintinline{python}{qbit}: índice do qubit que será medido.
  \end{itemize}

  \item \begin{minted}{python}
def measure_all(self)
  \end{minted}
  Mede todos os qubits do sistema.

  \item \begin{minted}{python}
def flip(self, gate, qbit, p)
  \end{minted}
  Aplica um erro de \textit{Bit}, \textit{Phase} ou \textit{bit-phase flip}. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{gate}: \mintinline{python}{str} com o valor
      \mintinline{python}{'x'}, \mintinline{python}{'z'} ou
      \mintinline{python}{'y'} para, respectivamente, os erros de \textit{Bit
      Flip}, \textit{phase flip} ou \textit{bit-phase flip};
    \item \mintinline{python}{qbit}: índice do qubit no qual será aplicado o erro;
    \item \mintinline{python}{p}: número entre 0 e 1 indicando a probabilidade
      do erro ocorrer.
  \end{itemize}

  \item \begin{minted}{python}
def amp_damping(self, qbit, p)
  \end{minted}
  Aplica um erro de decaimento de amplitude em um qubit. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{qbit}: índice do qubit no qual será aplicado o erro;
    \item \mintinline{python}{p}: número entre 0 e 1 indicando a probabilidade
      do erro ocorrer.
  \end{itemize}

  \item \begin{minted}{python}
def dpl_channel(self, qbit, p)
  \end{minted}
  Aplica o canal de despolarização em um qubit. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{qbit}: índice do qubit no qual será aplicado o erro;
    \item \mintinline{python}{p}: número entre 0 e 1 indicando a probabilidade
      do erro ocorrer.
  \end{itemize}

  \item \begin{minted}{python}
def print_state(self)
  \end{minted}
  Mostra na tela os valores não nulos da matriz densidade, no casso de estado
  misto, ou do vetor, no casso de estado puro, referente ao estado do sistema.

  \item \begin{minted}{python}
def get_size(self)
  \end{minted}
  Retorna o número de qubits criados na instanciação.

  \item \begin{minted}{python}
def get_bits(self)
  \end{minted}
  Retorna uma lista com os resultados das medidas dos qubits, sendo que 
  \mintinline{python}{None} indica que o qubit ainda não foi medido.

  \item \begin{minted}{python}
def change_to(self, state)
  \end{minted}
  Muda a representação do sistema para estado puro ou misto, sendo a mudança de
  estado puro $\ket{\psi}$, em representação vectorial, para o estado misto
  $\rho$, em matriz densidade, dada como $\rho =
  \ket{\psi}\mkern-7mu\bra{\psi}$. Porem na mudança de estado misto para estado
  puro a única garantia é que a probabilidade das medidas se mantem, pois
  diferentes estados podem resultar na mesma matriz densidade, como por exemplo
  \begin{align}
    \ket{1} \rightarrow &\begin{bmatrix} 0 \\ 1\end{bmatrix}\begin{bmatrix} 0 &
    1\end{bmatrix} &= \begin{bmatrix} 0 & 0 \\ 0 & 1\end{bmatrix} = \rho
        \label{eq:rhobegin}\\
    -\ket{1} \rightarrow& \begin{bmatrix} 0 \\ -1\end{bmatrix}\begin{bmatrix} 0 &
      -1\end{bmatrix} &= \begin{bmatrix} 0 & 0 \\ 0 & 1\end{bmatrix}= \rho\\
    i\ket{1} \rightarrow& \begin{bmatrix} 0 \\ i\end{bmatrix}\begin{bmatrix} 0 &
      -i\end{bmatrix} &= \begin{bmatrix} 0 & 0 \\ 0 & 1\end{bmatrix} = \rho\\
    -i\ket{1} \rightarrow& \begin{bmatrix} 0 \\ -i\end{bmatrix}\begin{bmatrix} 0 &
      i\end{bmatrix} &= \begin{bmatrix} 0 & 0 \\ 0 & 1\end{bmatrix} = \rho,
        \label{eq:rhoend}
  \end{align}
  dessa forma não é possivel diferenciar se $\rho$ foi gerado por $\ket{1}$,
  $-\ket{1}$, $i\ket{1}$ ou $-i\ket{1}$. O algoritmo usado para mudança de
  estado misto para estado puro é o seguinte: sendo $\rho_{ij}$ o elemento da
  linha $i$ coluna $j$ da matriz $\rho$ e $\ket{0}, \dots, \ket{2^n{-}1}$
  vetores na base computacional para $n$ qubits, o estado em vetor sesultante
  da mudança é
  \begin{align}
    \ket{\psi} = \sum_{i=0}^{2^n{-}1} \sqrt{\rho_{ii}}\ket{i}.
  \end{align}
  Seguindo o algoritmo, o estado $\rho$ das Equações~\ref{eq:rhobegin}
  à~\ref{eq:rhoend} resultaria no estado $\ket{1}$.

  \item \begin{minted}{python}
def get_state(self)
  \end{minted}
  Retorna a \mintinline{python}{str} \mintinline{python}{'mix'}, se o sistema estiver em estado
  misto, ou \mintinline{python}{'pure'} se o sistema estiver em estado puro.

  \item \begin{minted}{python}
def add_ancillas(self, an_num)
  \end{minted}
  Adiciona qubits ancilares. Parâmetro:
  \begin{itemize}
    \item \mintinline{python}{an_num}: número de ancilas a ser adicionada no sistema.
  \end{itemize}

  \item \begin{minted}{python}
def rm_ancillas(self)
  \end{minted}
  remove todas as ancilas do sistema.

  \item \begin{minted}{python}
def an_evol(self, gate, qbit)
  \end{minted}
  Aplica uma porta lógica quântica em um qubit ancilar. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{gate}: \mintinline{python}{str} de um caractere indicando a porta lógica que
      será apicada;
    \item \mintinline{python}{qbit}: índice do qubit ancilar no qual a porta
      será aplicada.
  \end{itemize}

  \item \begin{minted}{python}
def an_measure(self, qbit)
  \end{minted}
  Mede um qubit ancilar na base computacional. Parâmetro:
  \begin{itemize}
    \item \mintinline{python}{qbit}: índice do qubit ancilar que será medido.
  \end{itemize}

  \item \begin{minted}{python}
def get_an_size(self)
  \end{minted}
  Retorna o número de qubits ancilares no sistema.

  \item \begin{minted}{python}
def get_an_bits(self)
  \end{minted}
  Retorna uma lista com os resultados das medidas dos qubits ancilares, sendo
  que \mintinline{python}{None} indica que o qubit ancilar ainda não foi
  medido.

\end{description}

\subsection{Classe \texttt{Gate}}

A classe \mintinline{python}{Gate} armazena as portas lógicas quânticas usadas
na computação, além das portas $X$, $Y$, $Z$, Hadamard, $S$ e $T$ já
disponíveis é possível usar os métodos abaixo para criar novas portar.

\begin{description}
  \item \begin{minted}{python}
def make_gate(self, u, m)
  \end{minted}
  Cria uma porta lógica de um qubit. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{u}: \mintinline{python}{str} de um caractere
      referente ao nome da nova porta lógica quântica.
    \item \mintinline{python}{m}: lista de \mintinline{python}{complex}
      referentes a matriz da nova porta lógica, na seguinte ordem
      \begin{minted}{python}
[m00, m01,
 m10, m11]
      \end{minted}
  \end{itemize}

  \item \begin{minted}{python}
def make_cgate(self, u, ops, control)
  \end{minted}
  Cria uma porta lógica controlada qualquer que afeta todo o sistema. Parâmetros:
  \begin{itemize}
    \item \mintinline{python}{u}: \mintinline{python}{str} referente ao nome da
      nova porta lógica controlada.
    \item \mintinline{python}{ops}: \mintinline{python}{str} de tamanho
      referente ao número total de qubits no sistema, incluindo ancilas, onde
      cada caractere diz respeito a aplicação de uma porta lógica no qubit de
      respectiva posição caso os qubits de controle estejam no estado
      $\ket{1}$.
    \item \mintinline{python}{control}: lista com índices dos qubits de controle.
  \end{itemize}
\end{description}

\section{Portas lógicas}\label{sec:qsysporta}

Os métodos \mintinline{python}{evol} (listados na
Subsecção~\ref{sub:qsysmetodos}) aplicam as portas logicas armazenadas na
instancia da classe \mintinline{python}{Gate} passada durante a instanciação.

\subsection{Portas de um qubit}

A classe \mintinline{python}{Gate} possui algumas portas lógicas quânticas de
um qubit pré configuradas, que são listadas na Tabela~\ref{tab:qsysportas}.
Toda porta de um qubit é representada por uma \mintinline{python}{str} de um
caractere. Uma demostração demostração do uso de portas de um qubit pode ser
visto no Exemplo de código~\ref{code:evol}.

\begin{table}[h]
  \centering
  \begin{tabular}{lcl}
    \toprule
    Porta lógica & \mintinline{python}{str} & Referencia\\
    \midrule
    Porta identidade &  \mintinline{python}{'I'} &Subsecção~\ref{subsubsec:sigmai}\\
    Porta $X$ &  \mintinline{python}{'X'} &Subsecção~\ref{subsubsec:sigmax}\\
    Porta $Z$ &  \mintinline{python}{'Z'} &Subsecção~\ref{subsubsec:sigmaz}\\
    Porta $Y$ &  \mintinline{python}{'Y'} &Subsecção~\ref{subsubsec:sigmay}\\
    Porta Hadamard &  \mintinline{python}{'H'} &Subsecção~\ref{subsubsec:hadamardgate}\\
    Porta $S$ &  \mintinline{python}{'S'} &Subsecção~\ref{subsubsec:sgate}\\
    Porta $T$ &  \mintinline{python}{'T'} &Subsecção~\ref{subsubsec:tgate}\\
    \bottomrule
  \end{tabular}
  \caption{Portas lógicas quântica pre configuradas na classe
  \mintinline{python}{Gate}.}\label{tab:qsysportas}
\end{table}


\begin{listing}[h]
  \caption{Aplicação de portas lógicas de um qubit no sistema.}\label{code:evol}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate = Gate()
q = QSystem(nqbits=3, seed=42, gate=gate, state='mix')
q.evol('H', 0, 3) # Aplica a porta Hadamard nos qubit 0, 1 e 2
q.evol('Z', 2)    # Aplica a porta X no qubit 2, terceiro qubit
q.evol('XYZ')     # Aplica as portas X, Y e Z nos qubits 0,1 e 2
\end{minted}
\end{tcolorbox}
\end{listing}

É possível criar novas portas logicas com o método
\mintinline{python}{make_gate} da classe \mintinline{python}{Gate},
como por exemplo a porta $S^\dagger$ descrita pela matriz
\begin{align}
  \begin{bmatrix}
    1 & 0 \\
    0 & -i
  \end{bmatrix}
\end{align}
pode ser criada com o seguinte comando:
\begin{minted}{python}
gate.make_gate('d', [1, 0, 
                     0, -1j])
\end{minted}
onde o literal \mintinline{python}{j} representa a contante $i = \sqrt{-1}$ e
gera um objeto da classe \mintinline{python}{complex}. Após criada a porta
logica pode ser usada como qualquer outra porta lógica de um qubit, como visto 
no Exemplo de código~\ref{code:gatemake}.


\begin{listing}[h]
  \caption{Criação e utilização de portas lógicas de um qubit.}\label{code:gatemake}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate = Gate()
gate.make_gate('d', [1, 0, 0, -1j]) # Cria a porta S†
q = QSystem(nqbits=2, seed=42, gate=gate, state='pure')
q.evol('X', 0) # Aplica a porta X no qubit 0, primeiro qubit
q.evol('d', 0) # Aplica a porta S† no qubit 0, primeiro qubit
\end{minted}
\end{tcolorbox}
\end{listing}

\subsection{Portas controladas}

A porta \mintinline{python}{cnot} é aplicada pelo método de mesmo nome da
classe \mintinline{python}{QSystem}, mas como é possível escolher um numero
arbitrário de qubits de controle o método \mintinline{python}{cnot} implementa
tanto uma porta \mintinline{python}{cnot} quanto uma porta de
Toffoli\footnote{A porta Toffoli é basicamente uma porta
\mintinline{python}{cnot} com dois qubits de controle.}, como pode ser visto no
Exemplo de código~\ref{code:cnot}.

\begin{listing}[h]
  \caption{Aplicação da porta \mintinline{python}{cnot} e Toffoli no
  sistema.}\label{code:cnot}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate = Gate()
q = QSystem(nqbits=3, seed=42, gate=gate, state='mix')
q.cnot(0, [1])    # cnot com o qubit 0 como alvo e o 1 como controle
q.cnot(0, [1, 2]) # Toffoli com o 0 como alvo e o 1 e 2 como controle
\end{minted}
\end{tcolorbox}
\end{listing}

\subsection{Criação de portas controladas}

É possível criar portas controladas quaisquer utilizando o método
\mintinline{python}{make_cgate} da classe \mintinline{python}{Gate}, para isso
basta ter a listo dos qubits de controle e a \mintinline{python}{str}, de
tamanho referente ao número total de qubits no sistema incluindo ancilas, onde
cada caractere diz respeito a aplicação de uma porta lógica no qubit de
respectiva posição caso os qubits de controle estejam no estado $\ket{1}$. Para
aplicara a porta controlada criado é usado o método \mintinline{python}{cevol}
da classe \mintinline{python}{QSystem}, como é visto no Exemplo de
código~\ref{code:makecgate}.

\begin{listing}[h]
  \caption{Criação e utilização de portas controladas.}\label{code:makecgate}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate1, gate2 = Gate(), Gate()
gate1.make_cgate(name='ck1', gates='XXI', control=[2]) # Cria porta 
gate2.make_cgate(name='ck1', gates='XIX', control=[2]) # controlada
q = QSystem(nqbits=3, seed=42, gate=gate1, state='pure')
q.cevol('ck1')        # Aplica a porta controlada ck1 do gate1
q.cevol('ck1', gate2) # Aplica a porta controlada ck1 do gate2
\end{minted}
\end{tcolorbox}
\end{listing}

\section{Operação de medida}\label{sec:qsysmedida}

As medidas, na base computacional, são feitas pelos métodos
\mintinline{python}{measure} e \mintinline{python}{measure_all} da classe
\mintinline{python}{QSystem}, os resultados das medidas são armazenadas em um
lista, acessível pelo método \mintinline{python}{get_bits}, onde cada posição
representa seu respectivo qubit. Para os qubits que ainda não foram medidos há
o valor \mintinline{python}{None} na sua respectiva posição da lista de medidas
e para os que já foram medidos há o valor 0 ou 1 dependendo do resultado da
ultima medida, como pode ser visto no Exemplo de código~\ref{code:medida}

\begin{listing}[h]
  \caption{Os resultados das medidas são \mintinline{python}{bits1 = [0, 0,
  None]} e \mintinline{python}{bits2 = [1, 0, 0]}.}\label{code:medida}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate = Gate()
q = QSystem(nqbits=3, seed=42, gate=gate, state='mix')
q.evol('H', 0)       # Circuito 
q.cnot(1, [0])       # de Bell
q.measure(0)         # Mede o qubit 0 na base computacional
q.measure(1)         # Mede o qubit 1 na base computacional
bits1 = q.get_bits() # Retorna o resultado das medidas
q.evol('HHH')        # Aplica Hadamard em todos os qubits
q.measure_all()      # Mede todos os qubits na base computacional
bits2 = q.get_bits() # Retorna o resultado das medidas
\end{minted}
\end{tcolorbox}
\end{listing}

\section{Aplicação de canais de erro}\label{sub:qsyserro}

Os canais de erros apresentados na Secção~\ref{sec:erro} são implementados como
métodos da classe \mintinline{python}{QSystem}, conforme é visto na sequencia.

\subsection{Canais de \textit{Bit}, \textit{Phase} e \textit{bit-phase flip}}

Os canais de \textit{Bit}, \textit{Phase} e \textit{bit-phase flip} são todos
implementados no mesmo método \mintinline{python}{flip}, de forma que o
parâmetro \mintinline{python}{gate} indica o tipo de erro conforme a
Tabela~\ref{tab:gateerro}, e uma possív

\begin{table}[h]
  \centering
  \begin{tabular}{lc}
    \toprule
    Erro & \mintinline{python}{gate}\\
    \midrule
    \textit{bit flip} & \mintinline{python}{'X'} \\
    \textit{phase flip}& \mintinline{python}{'Z'} \\
    \textit{bit-phase flip}& \mintinline{python}{'Y'} \\
    \bottomrule
  \end{tabular}
  \caption{Canal de erro referente ao parâmetro \mintinline{python}{gate} do
  método \mintinline{python}{flip} da classe
  \mintinline{python}{QSystem}.}\label{tab:gateerro}
\end{table}

Os canais de \textit{Flip} podem ser aplicados tanto em estado puro quanto em
estado misto, onde a aplicação em estado misto se dá da forma vista nas
Subsecções~\ref{subsec:bitfliperro} à~\ref{subsec:bitphasefliperro}. Já a
implementação do método \mintinline{python}{flip} para estado puro  pode ser
feita da seguinte código em Python:
\begin{minted}{python}
def flip(self, gate, qbit, p):
    if not p == 0 and p >= random.random():
        self.evol(qbit, gate)
\end{minted}

\begin{listing}[h]
  \caption{Erros de \textit{Bit}, \textit{Phase} e \textit{bit-phase flip}.}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate = Gate()
q = QSystem(nqbits=5, seed=42, gate=gate, state='pure')
q.flip('X', 3, 0.13) # bit flip no qubit 3 com probabilidade de 13%
q.change_to('mix')   # Muda para estado misto
q.flip('Z', 4, 0.05) # Phase Flip no qubit 4 com probabilidade de 5%
q.flip('Y', 2, 0.76) # Bit-Phase Flip no 2 com probabilidade de 76%
\end{minted}
\end{tcolorbox}
\end{listing}

\subsection{Canal de despolarização e decaimento de amplitude}

Os canais de despolarização e de decaimento de amplitude são implementados,
respectivamente, pelos métodos \mintinline{python}{dpl_channel} e
\mintinline{python}{amp_damping} da classe \mintinline{python}{QSystem}, porem
ao contrario do método \mintinline{python}{flip}, que atua tanto em estado puro
e estado misto, esses métodos atuam apenas em estado misto.

\begin{listing}[h]
  \caption{Erros de \textit{Bit}, \textit{Phase} e \textit{bit-phase flip}.}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate = Gate()
q = QSystem(nqbits=5, seed=42, gate=gate, state='mix')
q.dpl_channel(1, 0.26) # Canal de despolarização atuando no
                       # qubit 1 com probabilidade de 26%
q.amp_damping(3, 0.47) # Canal de decaimento de amplitude atuando
                       # no qubit 3 com probabilidade de 47%
\end{minted}
\end{tcolorbox}
\end{listing}

\section{Utilização de qubits ancilares}\label{sec:qsysancila}

O simulador permite adicionar e remover qubits ancilares durante a computação.
Utilizando o método \mintinline{python}{add_ancillas} é possível adicionar um
número arbitrário de qubits ancilares, os novos qubits são indexados de 0 ao
numero de ancilas -1 e são manipulados apenas pelos métodos
\mintinline{python}{an_evol} e  \mintinline{python}{an_measure} (o método
\mintinline{python}{measure_all} também mede os qubits ancilares). O resultado
das medidas dos qubits ancilares, da mesma forma que os demais qubits, são
armazenados em uma lista que é acessível pelo método
\mintinline{python}{get_an_bits}.

O método \mintinline{python}{evol} que recebe apenas uma
\mintinline{python}{str} e as portas lógicas controladas criadas pelo método
\mintinline{python}{make_cgate} levam em consideração os qubits ancilares,
sendo eles adicionados a direita, como pode ser visto no Exemplo de
código~\ref{code:ancilla}.

\begin{listing}[h]
  \caption{Código de \textit{bit flip} utilizando estabilizadores, a síndrome
  aponta um erro no segundo qubit
  (\mintinline{python}{syn = [1, 0]}).}\label{code:ancilla}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate = Gate()
#                              ┌Estabilizadores
#                             ┌┴┐      ┌Qubit ancilar 1
gate.make_cgate('ck1', gates='IZZII', [3]) # Serão 3 qubits
gate.make_cgate('ck2', gates='ZIZII', [4]) # instanciados 
#                                └┤    └Qubit ancilar 2
#                                 └Ancilas
q = QSystem(nqbits=3, seed=44, gate=gate, state='mix') 
q.flip('X', 1, 0.34)  # bit flip no qubit 1
q.add_ancillas(2)     # Adiciona dois qubits ancilares
q.an_evol('H', 0)     # Aplica a porta Hadamard no qubit ancilar 0
q.an_evol('H', 1)     # Aplica a porta Hadamard no qubit ancilar 1
q.cevol('ck1')        # Aplica estabilizador IZZ
q.cevol('ck2')        # Aplica estabilizador ZIZ
q.an_evol('H', 0)     # Aplica a porta Hadamard no qubit ancilar 0
q.an_evol('H', 1)     # Aplica a porta Hadamard no qubit ancilar 1
q.an_measure(0)       # Medida da
q.an_measure(1)       # síndrome
syn = q.get_an_bits() # Erro no qubit 1
q.rm_ancillas()       # Remove os qubits ancilares
\end{minted}
\end{tcolorbox}
\end{listing}

Para remover as ancilas é utilizado o método \mintinline{python}{rm_ancillas}
da classe \mintinline{python}{QSystem}, esse método atua de maneira diferente
para estado puro e estado misto, para estado misto a remoção da ancila e feita
com uma operação de traço parcial, como vista na
Subsecção~\ref{subsec:tracoparcial}, já para estado puro os qubits ancilares
são medidos antes da remoção, afetando assim os outros qubits, esse efeito pode
ser visto no Exemplo de código~\ref{code:rmancillas}, onde o mesmo circuito
apresenta resultados diferentes para estado puro e misto.
\begin{listing}[h]
  \caption{Diferentes efeitos do método \mintinline{python}{rm_ancillas}: como
  a \mintinline{python}{seed} de \mintinline{python}{v} e
  \mintinline{python}{m} são iguais as medidas teriam os mesmos resultados se
  as operações focem equivalentes, porem \mintinline{python}{pure_bit = [1]} e
  \mintinline{python}{mix_bit = [0]}.}\label{code:rmancillas}
\begin{tcolorbox}
\begin{minted}[xleftmargin=0.5cm, linenos]{python}
from qsystem import QSystem, Gate
gate = Gate()
v = QSystem(1, 22, gate, state='pure') # Estado puro
v.evol('H', 0)    # 1/sqrt(2) (|0>+|1>)
v.add_ancillas(1) # 1/sqrt(2) (|00>+|10>)
v.cnot(1, [0])    # 1/sqrt(2) (|00>+|11>)
v.rm_ancillas()   #            |1>             &  mede ancilas
v.evol('H')       # 1/sqrt(2) (|0>-|1>)
v.measure(0)      # Mede 1, p(0) = 0.5 e p(1) = 0.5
pure_bit = v.get_bits()
m = QSystem(1, 22, gate, state='mix') # Estado misto
m.evol('H', 0)    # 0.5 (|0>+|1>)(<0|+<1|)
m.add_ancillas(1) # 0.5 (|00>+|10>)(<00|+<10|)
m.cnot(1, [0])    # 0.5 (|00>+|10>)(<00|+<10|)
m.rm_ancillas()   # 0.5 (|0>+|1>)(<0|+<1|)     & traço parcial 
m.evol('H')       #      |0><0|
m.measure(0)      # Mede 0, p(0) = 1
mix_bit = m.get_bits()
\end{minted}
\end{tcolorbox}
\end{listing}

\section{Exemplos de código}\label{sec:qsysexemplos}
A seguir serão apresentados dois exemplos com os circuitos de preparação e
correção de erros dos códigos de Steane $[[7,1,3]]$ e de 5-qubits $[[5,1,3]]$.

\subsection{Algoritmo de Shor}

\subsection{Código de 7-qubits}
%
%O código que será apresentado pode ser visto por completo no
%Apêndice~\ref{subsec:steane.py}. Os circuitos de preparação e correão são
%basicamente os mesmo para o código de Steane, ambos utilizam portas controladas
%que são descritas pelos estabilizadores do código
%(Secção~\ref{sec:estabilizador}), sendo assim, primeiramente são criadas as
%portas controladas \mintinline{python}{'ck1'},  \mintinline{python}{'ck2'},
%\mintinline{python}{'ck3'}, \mintinline{python}{'ck4'},
%\mintinline{python}{'ck5'} e \mintinline{python}{'ck6'}.
%
%\inputminted[firstline=7, lastline=12, linenos,
%xleftmargin=1cm]{python}{../qsystem/app/steane_code.py}
%
%Note que que é adicionado o operador \mintinline{python}{'I'} ao final de cada
%estabilizador, pois são usadas ancilas para fazer as operações controladas e
%elas precisão ser consideradas, da mesma forma, o qubit de controle é o 7, ou
%seja o primeiro qubit ancilar já que o estado só terá sete qubits. O circuito
%usado para preparar e corrigir o error é similar ao circuito da
%Figura~\ref{fig:7prep}, porem devido ao fato que o tempo computação é
%exponencial em relação ao numero de qubits, é aplicado/medido um estabilizador
%por vez, como definido na função \mintinline{python}{prepare}.
%
%\inputminted[firstline=14, lastline=27, linenos,
%xleftmargin=1cm]{python}{../qsystem/app/steane_code.py}
%
%Ao final da função \mintinline{python}{prepare} é retornado uma lista com as
%medidas dos qubits ancilares, ou seja, a medida da síndrome. Como essa função é
%implementado de maneira genérica, outras duas funções são definidos, sendo
%elas: \mintinline{python}{prepare_z} usado para preparar o estado e corrigir o
%erro de \textit{phase flip}; e o \mintinline{python}{prepare_x} usado para
%corrigir o erro de \textit{bit flip}.
%
%\inputminted[firstline=29, lastline=33, linenos,
%xleftmargin=1cm]{python}{../qsystem/app/steane_code.py}
%
%Para testar o código estabilizador são executadas mil rodadas de codificação,
%aplicação de erro e correção de erro.
%
%\inputminted[firstline=37, lastline=39, linenos,
%xleftmargin=1cm]{python}{../qsystem/app/steane_code.py}
%
%Aleatoriamente, em dois qubits são aplicados os erros de \textit{bit flip} e
%\textit{phase flip} com valor aleatório de $p$.
%
%\inputminted[firstline=40, lastline=45, linenos,
%xleftmargin=1cm]{python}{../qsystem/app/steane_code.py}
%
%São feitas as correções e o estado é medido para verificar a paridade, por 
%fim são mostrados na tela os resultados das medidas
%da preparação do estado, da correção de \textit{phase} e \textit{bit flip}, do sistema
%corrigido, e qubit lógico medido, que que 
%
%\subsection{Código de 5-qubits}
%\inputminted{python}{../qsystem/app/5code.py}
%
\chapter{Implementação do simulador QSystem}\label{ch:imple}
\todo{Será especificado em TCC 2.}
\section{Arquitetura do software}
\section{Definição da classe Gate}
\section{Definição da classe QSystem}
\subsection{Atributos}
\subsection{Métodos}
\section{Portas lógicas e evolução do sistema}
\subsection{Portas de um qubit}
\subsection{Porta \texttt{cnot}}
\subsection{Porta \texttt{cphase}}
\subsection{Porta \texttt{swap}}
\subsection{Porta \texttt{qft}}
\subsection{Criação de portas controlada}
\section{Implementação da operação de medida}
\section{Implementação dos canais de erro}
\subsection{Canais de \textit{Bit}, \textit{Phase} e \textit{bit-phase flip}}
\subsection{Canal de despolarização}
\subsection{Canal de decaimento de amplitude}
\section{Qubits ancilares}
\subsection{Adicionar ancilas}
\subsection{Remover ancilas}
\subsection{Evolução e medida}
\section{Interface \texttt{C++/}Python}

\chapter{Simulação e coleta de dados}
\todo{Será especificado em TCC 2.}
