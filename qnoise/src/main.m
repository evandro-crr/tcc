[x y z] = sphere(20);
rho = bloch_sphere(x, y, z);

[X,Y] = meshgrid(1:size(x));
C = mod(X,11)(:,1:10);
C = [C fliplr(C) zeros(21, 1)];
X = C;
Z = Y;

#plot_bloch(@bit_flip, rho, "Bit Flip", 0.85, Z)
#plot_bloch(@phase_flip, rho, "Phase Flip", 1, X)
#plot_bloch(@bit_phase_flip, rho, "Bit-phase Flip", 0.75, Z)
#plot_bloch(@depolarizing_channel, rho, "Depolarizing Channel", 0.9, Z)
plot_bloch(@amplitude_damping, rho, "Amplitude Damping", 0.9, Z)

