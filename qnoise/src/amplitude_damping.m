function rho = amplitude_damping(rho, gamma)
  E0 = [1             0;
        0 sqrt(1-gamma)];
  E1 = [0 sqrt(gamma);
        0          0];
  
  for i = 1 : length(rho)
    rho{i} = E0*rho{i}*E0' + E1*rho{i}*E1';
  end
end
