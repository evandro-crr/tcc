function rho = phase_flip(rho, p)
  Z = [1 0;
       0 -1];

  E0 = sqrt(p)*eye(2);
  E1 = sqrt(1-p)*Z;
  
  for i = 1 : length(rho)
    rho{i} = E0*rho{i}*E0' + E1*rho{i}*E1';
  end
end
