function [x y z] = expected_values(rho)
  pauli_matrices
  x = [];
  y = [];
  z = [];
  for i = 1 : length(rho)
    x = [x trace(X*rho{i})];
    y = [y trace(Y*rho{i})];
    z = [z trace(Z*rho{i})];
  end
end
