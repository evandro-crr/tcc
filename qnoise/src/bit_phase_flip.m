function rho = bit_phase_flip(rho, p)
  Y = [0 -i;
       i 0];

  E0 = sqrt(p)*eye(2);
  E1 = sqrt(1-p)*Y;
  
  for i = 1 : length(rho)
    rho{i} = E0*rho{i}*E0' + E1*rho{i}*E1';
  end
end
