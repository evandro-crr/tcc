function rho = depolarizing_channel(rho, p)
  for i = 1 : length(rho)
    rho{i} = p*eye(2)/2 + (1-p)*rho{i};
  end
end
