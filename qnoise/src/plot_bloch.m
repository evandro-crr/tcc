function plot_bloch(E, rho, name, p, color)
  rho = E(rho, p);
  [ex ey ez] = expected_values(rho);
  n = sqrt(size(ex))(2);
  for i = 1:n
    for j = 1:n
    exi(i,j) = ex(i+(j-1)*n);  
    eyi(i,j) = ey(i+(j-1)*n);  
    ezi(i,j) = ez(i+(j-1)*n);  
    end
  end
  surf(exi, eyi, ezi, color);
  title(strcat(name, " : p = ", mat2str(p)));
  axis equal
  xlabel("x")
  ylabel("y")
  zlabel("z")
  axis([-1 1 -1 1 -1 1])
end
