function rho = bit_flip(rho, p)
  X = [0 1;
       1 0];

  E0 = sqrt(p)*eye(2);
  E1 = sqrt(1-p)*X;
  
  for i = 1 : length(rho)
    rho{i} = E0*rho{i}*E0' + E1*rho{i}*E1';
  end
end
