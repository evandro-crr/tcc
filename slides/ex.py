from qsystem import QSystem, Gates
from random import randint, seed
from math import log2, ceil, gcd
to_int = lambda c : sum(
        [m*2**i for m, i in zip(
            c, reversed(range(len(c))))])
seed(47) 

n = 15
a = 0
while gcd(n, a) != 1:
    a = randint(2, n)

s = ceil(log2(n+1)) 
gates = Gates()
def pown(x):
    x = x >> s
    fx = pow(a, x, n)
    return (x << s) | fx
def it():
    for x in range(2**s):
        yield x << s
gates.make_fgate('POWN', pown, 2*s, it()) 


q = QSystem(s, gates, 13) 
q.evol('H', 0, s)
q.add_ancillas(s)
q.evol('POWN', 0)
q.measure(s, s)
q.rm_ancillas()
q.qft(0, s)

q.measure_all()
c = to_int(q.bits())

q = QSystem(s, gates, 13) <-
q.evol('H', 0, s)
q.add_ancillas(s)
q.evol('POWN', 0)
q.measure(s, s)
q.rm_ancillas()
q.qft(0, s)

q.measure_all()
c = to_int(q.bits())

q = QSystem(s, gates, 13) 
q.evol('H', 0, s) <-
q.add_ancillas(s)
q.evol('POWN', 0)
q.measure(s, s)
q.rm_ancillas()
q.qft(0, s)

q.measure_all()
c = to_int(q.bits())

q = QSystem(s, gates, 13) 
q.evol('H', 0, s)         
q.add_ancillas(s) <-
q.evol('POWN', 0)
q.measure(s, s)
q.rm_ancillas()
q.qft(0, s)

q.measure_all()
c = to_int(q.bits())

q = QSystem(s, gates, 13) 
q.evol('H', 0, s)         
q.add_ancillas(s) 
q.evol('POWN', 0) <-
q.measure(s, s)
q.rm_ancillas()
q.qft(0, s)

q.measure_all()
c = to_int(q.bits())

q = QSystem(s, gates, 13) 
q.evol('H', 0, s)         
q.add_ancillas(s) 
q.evol('POWN', 0) 
q.measure(s, s) <-
q.rm_ancillas()
q.qft(0, s)

q.measure_all()
c = to_int(q.bits())

q = QSystem(s, gates, 13) 
q.evol('H', 0, s)         
q.add_ancillas(s) 
q.evol('POWN', 0) 
q.measure(s, s) 
q.rm_ancillas() <-
q.qft(0, s)

q.measure_all()
c = to_int(q.bits())

q = QSystem(s, gates, 13) 
q.evol('H', 0, s)         
q.add_ancillas(s) 
q.evol('POWN', 0) 
q.measure(s, s) 
q.rm_ancillas() 
q.qft(0, s) <-

q.measure_all()
c = to_int(q.bits())

q = QSystem(s, gates, 13) 
q.evol('H', 0, s)         
q.add_ancillas(s) 
q.evol('POWN', 0) 
q.measure(s, s) 
q.rm_ancillas() 
q.qft(0, s) 

q.measure_all() <-
c = to_int(q.bits())

mea = [c]
for _ in range(s-1):
    se = randint(210,760)
    q = QSystem(s, gates, se)
    q.evol('H', 0, s)
    q.add_ancillas(s)
    q.evol('POWN', 0)
    q.rm_ancillas()
    q.qft(0, s)
    q.measure_all()
    c = to_int(q.bits())
    mea.append(c)

c = mea[0]
for m in mea:
    c = gcd(c, m)
if c == 0:
    print('Erro')
else:
    r = 2**s/c
    if r % 2 == 1:
        print('Erro')
    else:
        p = gcd(int(a**(r/2)+1), n)
        q = gcd(int(a**(r/2)-1), n)
print(p, q)

